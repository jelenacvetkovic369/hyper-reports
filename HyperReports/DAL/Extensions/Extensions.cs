﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HyperReports.DAL.Extensions
{
    public static class Extensions
    {
        public static bool HasAttribute<AttributeType>(this PropertyInfo prop)
            where AttributeType : Attribute
        {
            var attributes = prop.GetCustomAttributes(typeof(AttributeType));

            return attributes.Any();
            
        }

    }
}
