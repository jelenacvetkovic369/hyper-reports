﻿using HyperReports.DAL.Model;
using HyperReports.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace HyperReports.DAL.Repositories
{
    public class StoreRepository : BaseRepository<Store, StoreXml>
    {
        public StoreRepository(string connectionstring)
            : base(connectionstring) { }

    }
}
