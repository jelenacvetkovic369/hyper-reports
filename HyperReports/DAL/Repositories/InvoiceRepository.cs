﻿using HyperReports.Model;
using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using HyperReports.DAL.Model;

namespace HyperReports.DAL.Repositories
{
    public class InvoiceRepository : BaseRepository<Invoice, InvoiceXml>
    {
        public InvoiceRepository(string connectionstring)
            : base(connectionstring) { }

        public override Invoice Convert(InvoiceXml xmlEntity)
        {
            Invoice inv=base.Convert(xmlEntity);
            inv.CustomerUuid = xmlEntity.CustomerDetails.Uuid; 
            
            return inv;
        }
       
    }
}
