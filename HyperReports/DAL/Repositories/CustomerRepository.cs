﻿using HyperReports.DAL.Model;
using HyperReports.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.DAL.Repositories
{
    public class CustomerRepository : BaseRepository<Customer, CustomerXml>
    {
        public CustomerRepository(string connectionstring)
            : base(connectionstring) { }
    }
}
