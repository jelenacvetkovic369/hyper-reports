﻿using ClosedXML.Excel;
using HyperReports.DAL.Model;
using HyperReports.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Configuration;
using System.Linq;

namespace HyperReports.DAL.Repositories
{
    public class CompanyRepository : BaseRepository<Company,CompanyXml>
    {
        public CompanyRepository(string connectionstring)
            : base(connectionstring) { }


        public string CreateQueryString(int topN, int[] months, string aggregation, string companyName, int year, string order)
        {
            string query = "SELECT ";

            string groupBy = "GROUP BY Uuid", whereMonth = "", orderBy = "";
            CompanyXml comp = new CompanyXml(); comp.Name = companyName;
            int companyUuid = base.GetIdIfExists(comp, "Name");

            if (!string.IsNullOrEmpty(order))
                orderBy = " ORDER BY Total " + order;

            if (topN != 0)
                query += $"TOP {topN}";

            switch (aggregation)
            {
                case "store":
                    query += " (SELECT Name FROM Store WHERE Uuid=StoreUuid) As Store";
                    groupBy = "GROUP BY StoreUuid";
                    break;
                case "payment":
                    query += " Payment";
                    groupBy = "GROUP BY Payment";
                    break;
                case "invoice":
                    query += " Uuid AS Invoice";
                    break;
                case "receipt":
                    query += " Uuid AS Receipt";
                    break;
            }

            if (months.Length > 0)
            {
                whereMonth = $" AND month(Date) IN( {string.Join(", ", months)}) ";
                foreach (int month in months)
                {
                    string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                    query += $", SUM(CASE WHEN month(Date) = {month} THEN Total END) {monthName}";
                }
            }

            query += ", SUM(Total) AS Total " 
                        + "FROM (SELECT Uuid, Total,Date, Payment, StoreUuid, CardUuid FROM Receipt"
                        + " UNION ALL"
                        + " SELECT Uuid, Total, Date, Payment, StoreUuid, CardUuid FROM Invoice) AS A"
                         + $" WHERE StoreUuid IN(SELECT Uuid FROM Store WHERE CompanyUuid = {companyUuid})";

            query += $" AND year(Date)={year}" + whereMonth;
            query += groupBy + orderBy;

            return query;
        }

        public DataTable GetTurnover(string query)
        {
            using SqlConnection connection = new SqlConnection(_connectionstring);
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;

            connection.Open(); 
            string res = "";
            using (SqlDataAdapter myDataAdapter = new SqlDataAdapter(query, connection))
            {
                DataTable dtResult = new DataTable();
                myDataAdapter.Fill(dtResult);

                if (dtResult.Columns[0].ColumnName == "Payment")
                {
                    DataRow rowCard = dtResult.AsEnumerable().SingleOrDefault(r => r.Field<string>("Payment") == "0");
                    DataRow rowCash = dtResult.AsEnumerable().SingleOrDefault(r => r.Field<string>("Payment") == "1");
                    rowCard[0] = "Card";
                    rowCash[0] = "Cash";
                }

                return dtResult;
            }
            
        }
    }
}
