﻿using HyperReports.DAL.Attributes;
using HyperReports.DAL.Entities;
using HyperReports.DAL.Extensions;
using HyperReports.DAL.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HyperReports.DAL.Repositories
{
    public abstract class BaseRepository<TEntity, TEntityXml> : IBaseRepository<TEntity,TEntityXml> where TEntity : BaseEntity, new()
    {
        protected readonly string _connectionstring;

        protected BaseRepository(string connectionstring)
        {
            _connectionstring = connectionstring;
        }

        public virtual TEntity Convert(TEntityXml xmlEntity)
        {
            Type entityType = typeof(TEntityXml);
            TEntity t = new TEntity();
           
            Type sourceType = typeof(TEntityXml);
            Type targetType = typeof(TEntity);

            foreach (PropertyInfo p in sourceType.GetProperties())
            {
                //  Get the matching property in the destination object
                PropertyInfo targetObj = targetType.GetProperty(p.Name);
                if (targetObj != null)
                {
                    //  Set the value in the destination
                    targetObj.SetValue(t, p.GetValue(xmlEntity, null), null);
                }
            }
            return t;
        }

        public virtual int Insert(TEntity entity)
        {
            using SqlConnection connection = new SqlConnection(_connectionstring);
            Type entityType = typeof(TEntity);

            string tableName = entityType.Name;
            IEnumerable<string> columns = entityType
                .GetProperties()
                .Where(prop => prop.GetValue(entity) != null)
                .Where(prop => !prop.HasAttribute<DoNotInsert>())
                .Select(prop => prop.Name);

            string columnsQueryVariable = string.Join(",", columns); 

            IEnumerable<string> values = columns.Select(col => $"@{col}");
            string valuesQueryVariable = string.Join(",", values);


            string query =                
                $@"INSERT INTO {tableName} ({columnsQueryVariable}) OUTPUT INSERTED.uuid VALUES ({valuesQueryVariable})";
           
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            var parameters = GetParameters(entity);
            command.Parameters.AddRange(parameters);

            connection.Open();
            return (int)command.ExecuteScalar();
        }



        protected SqlParameter[] GetParameters(TEntity entity)
        {
            IEnumerable<PropertyInfo> properties = typeof(TEntity)
                .GetProperties()
                .Where(prop => prop.GetValue(entity) != null)
                .Where(prop =>!prop.HasAttribute<DoNotInsert>());

            List<SqlParameter> parameters = new List<SqlParameter>();

            foreach (var prop in properties)
            {
                SqlParameter param = new SqlParameter();
                param.Value = prop.GetValue(entity);
                param.ParameterName = $"@{prop.Name}";
                parameters.Add(param);
            }

            return parameters.ToArray();
        }

      

        protected virtual void PopulateEntity(TEntity entity, IDataReader reader)
        {
            IEnumerable<PropertyInfo> properties = typeof(TEntity)
                .GetProperties();

            foreach (var property in properties)
            {
                object value = reader[property.Name];

                if (value.GetType() == typeof(DBNull))
                    value = null;

                property.SetValue(entity, value);
            }
        } 

        public int GetIdIfExists(TEntityXml xmlEntity, string identifier)
        {
            Type entityTypeTable = typeof(TEntity);
            Type entityType = typeof(TEntityXml);
            string tableName = entityTypeTable.Name;

            var identifierValue = entityType.GetProperty(identifier).GetValue(xmlEntity);

            using SqlConnection connection = new SqlConnection(_connectionstring);
            string query = $"SELECT uuid FROM {tableName} WHERE " + identifier + "=@Val";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;

            SqlParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Val";
            parameter.Value = identifierValue;
            command.Parameters.Add(parameter);

            connection.Open();

            object result = command.ExecuteScalar();
            int output = (result == null) ? -1 : (int)result;
            return output;
        }

    }
}
