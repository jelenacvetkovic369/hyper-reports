﻿using HyperReports.DAL.Model;
using HyperReports.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace HyperReports.DAL.Repositories
{
    public class CardRepository : BaseRepository<Card,CardXml>
    {
        public CardRepository(string connectionstring)
            : base(connectionstring) { }

        
    }
}
