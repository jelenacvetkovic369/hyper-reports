﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.DAL
{
    public static class Enumeration
    {
        public enum PaymentType
        {
            [XmlEnum(Name = "card")]
            Card,
            [XmlEnum(Name = "cash")]
            Cash
        }
    }
}
