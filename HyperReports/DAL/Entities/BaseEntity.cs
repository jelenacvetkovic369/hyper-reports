﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HyperReports.DAL.Entities
{
    public abstract class BaseEntity
    {

        public virtual int Uuid { get; set; }

    }
}
