﻿using HyperReports.DAL.Attributes;
using HyperReports.DAL.Entities;
using HyperReports.DAL.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.Model
{
    
    public class Company : BaseEntity
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public Company() { }
        //public Company(int uuid, string name, string add)
        //{
        //    this.Uuid = uuid;
        //    this.Name = name;
        //    this.Address = add;
        //}

    }

}
