﻿using HyperReports.DAL;
using HyperReports.DAL.Attributes;
using HyperReports.DAL.Entities;
using HyperReports.DAL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.Model
{
    public class Invoice :  BaseEntity
    {

        [DoNotInsert]
        public override int Uuid { get; set; }

        public decimal Total { get; set; }

        public DateTime Date { get; set; }

        public Enumeration.PaymentType Payment { get; set; }

        public int? CardUuid { get; set; }

        public int CustomerUuid { get; set; }

        public int StoreUuid { get; set; }

        public Invoice() { }

    }
}
