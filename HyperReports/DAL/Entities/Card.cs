﻿using HyperReports.DAL.Attributes;
using HyperReports.DAL.Entities;
using HyperReports.DAL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.Model
{

    public class Card : BaseEntity
    {

        [DoNotInsert]
        public override int Uuid { get; set; }

        public string Cardtype { get; set; }

        public string Number { get; set; }

        public bool IsContactless { get; set; }

        public Card() { }

    }
}
