﻿using HyperReports.DAL.Entities;
using HyperReports.DAL.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.Model
{

    public class Customer : BaseEntity
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public Customer() { }

    }
}
