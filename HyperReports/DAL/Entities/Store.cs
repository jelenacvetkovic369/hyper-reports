﻿using HyperReports.DAL.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;
using HyperReports.DAL.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using HyperReports.DAL.Model;

namespace HyperReports.Model
{
    public class Store : BaseEntity
    {
        [DoNotInsert]
        public override int Uuid { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
        public int companyUuid { get; set; } 

        public Store() { }

    }
}
