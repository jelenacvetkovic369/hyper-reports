﻿using System;
using System.Collections.Generic;
using System.Text;
using WinSCP;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace HyperReports.BLL
{
    public class SftpDownloader
    {
        Options options = Settings.Options;
        string localDirectory = Settings.LocalDirectory;
        string lastStored = Settings.LastStoredFile;
        private string jsonPath = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")) + "\\appsettings.json";

        public void SetLastStoredFile(string value)
        {
            string json = File.ReadAllText(jsonPath);
            JObject jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json) as JObject;
            jsonObj["Files"]["lastStoredFile"] = value;
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(jsonPath, output);
        }

        public List<string> GetNewFiles()
        {
            if (string.IsNullOrEmpty(localDirectory))
                throw new ArgumentNullException("LocalDirectory","Local directory path must be set!");

            SessionOptions sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = options.Host,
                UserName = options.Username,
                Password = options.Password,
                PortNumber = options.Port,
                GiveUpSecurityAndAcceptAnySshHostKey = true
            };
            
            using (Session session = new Session())
            {
                session.Open(sessionOptions);

                List<string> newFiles = new List<string>();
                
                List<string> files =
                    session.EnumerateRemoteFiles(
                        options.RemoteDirectory, "*.xml", WinSCP.EnumerationOptions.AllDirectories)
                        .Select(fileInfo => fileInfo.FullName)
                        .ToList();
                files.Sort();

                int lastDownloadedIndex = files.IndexOf(lastStored);
                if (lastDownloadedIndex == -1)
                {
                    session.SynchronizeDirectories(SynchronizationMode.Local, localDirectory, options.RemoteDirectory, true);
                    SetLastStoredFile(files.Last());
                    return files.Select(file=> localDirectory + "\\" + Path.GetFileName(file)).ToList();
                    
                }
                else
                {
                    for (int i = lastDownloadedIndex + 1; i < files.Count; i++)
                    {
                        newFiles.Add(localDirectory + "\\" + Path.GetFileName(files[i]));
                        session.GetFileToDirectory(files[i], localDirectory);
                    }

                    SetLastStoredFile(files.Last());
                    return newFiles;
                }
            }
        }
    }
}
