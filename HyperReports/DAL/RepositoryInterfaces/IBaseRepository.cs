﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.DAL.RepositoryInterfaces
{
    public interface IBaseRepository<TEntity,TEntityXml>
    {
        int Insert(TEntity entity);
        public TEntity Convert(TEntityXml xmlEntity);
        public int GetIdIfExists(TEntityXml xmlEntity, string identifier);
    }
}
