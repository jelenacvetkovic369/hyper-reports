﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.DAL.Model
{
    [XmlRoot("company")]
    public class CompanyXml
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("address")]
        public string Address { get; set; }

        [XmlAttribute("uuid")]
        public int Uuid { get; set; }

        [XmlArray("stores")]
        [XmlArrayItem("store")]
        public List<StoreXml> Stores { get; set; }

        public CompanyXml() { }

    }
}
