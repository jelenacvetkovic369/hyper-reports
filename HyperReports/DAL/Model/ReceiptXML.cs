﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.DAL.Model
{
    public class ReceiptXml
    {

        [XmlElement("total")]
        public decimal Total { get; set; }
        
        [XmlElement("datetime")]
        public DateTime Date { get; set; }

        [XmlElement("payment")]
        public Enumeration.PaymentType Payment { get; set; }

        [XmlElement("carddetails")]
        public CardXml CardDetails { get; set; }
    }
}
