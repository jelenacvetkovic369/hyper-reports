﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.DAL.Model
{
    public class CustomerXml
    {
        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("address")]
        public string Address { get; set; }

        [XmlElement("uuid")]
        public int Uuid { get; set; }
    }
}
