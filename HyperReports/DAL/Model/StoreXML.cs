﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.DAL.Model
{
    public class StoreXml
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("address")]
        public string Address { get; set; }

        [XmlArray("receipts")]
        [XmlArrayItem("receipt")]
        public List<ReceiptXml> Receipts { get; set; }

        [XmlArray("invoices")]
        [XmlArrayItem("invoice")]
        public List<InvoiceXml> Invoices { get; set; }

    }
}
