﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.DAL.Model
{
    public class InvoiceXml : ReceiptXml
    {

        [XmlElement("customer")]
        public CustomerXml CustomerDetails { get; set; }
    }
}
