﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.DAL.Model
{
    public class CardXml
    {
        [XmlElement("cardtype")]
        public string Cardtype { get; set; }

        [XmlElement("number")]
        public string Number { get; set; }

        [XmlElement("contactless")]
        public bool IsContactless { get; set; }
    }
}
