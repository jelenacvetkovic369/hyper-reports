﻿using System;
using Commander.NET;
using HyperReports.BLL.CommandLineInterface;

namespace HyperReports
{
    class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {

                string cmdline = Console.ReadLine();
                string[] arguments = cmdline.Split(" ");

                if (arguments[0] == "exit")
                    break;

                try
                {
                    CommanderParser<HyperReport> parser = new CommanderParser<HyperReport>();
                    HyperReport hr = parser.Add(arguments)
                                            .Parse();
                }
                catch (Commander.NET.Exceptions.CommandMissingException ex)
                {
                    Console.WriteLine("Command not recognized.");
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                        Console.WriteLine(ex.InnerException.Message);
                    else
                        Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
