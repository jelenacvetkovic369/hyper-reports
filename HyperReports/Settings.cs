﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace HyperReports
{
    public static class Settings
    {
        private static IConfiguration config;

        static Settings()
        {
            config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")))
                .AddJsonFile("appsettings.json")
                .Build();
        }

        public static string DefaultConnection
        {
            get
            {
                return config.GetConnectionString("DefaultConnection");
            }
        }

        public static Options Options {
            get {
                Options sessionOptions = new Options();
                config.Bind("SessionOptions", sessionOptions);
                return sessionOptions;
            }
        }
        public static string LastStoredFile
        {
            get { return config.GetSection("Files")["lastStoredFile"]; }
            
        }

        public static string LocalDirectory { get; set; }// = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")) + "\\Downloads";
         public static string ExportDirectory { get; set; }
    }

    public class Options
    { 
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string RemoteDirectory { get; set; }
    }
}
