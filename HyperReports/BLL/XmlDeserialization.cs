﻿using HyperReports.DAL.Model;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace HyperReports.BLL
{
    public class XmlDeserialization
    {
        string xmlFullPath;
        XmlDocument doc;
        XmlSerializer serializer;
        SftpDownloader downloader;

        public XmlDeserialization()
        {
            doc = new XmlDocument();
            serializer = new XmlSerializer(typeof(CompanyXml));
            downloader = new SftpDownloader();
        }

 
        public CompanyXml DeserializeFile(string xmlFile)
        {
            doc.Load(xmlFile);
            using (StringReader rdr = new StringReader(doc.InnerXml))
            {
                CompanyXml cxml = (CompanyXml)serializer.Deserialize(rdr);
                return cxml;
            }   
        }

        public List<CompanyXml> GetDataForDb() 
        {
            List<string> filesToInsert = downloader.GetNewFiles().Where(file => new FileInfo(file).Length > 0).ToList();
            List<CompanyXml> companies = new List<CompanyXml>();

            foreach (string file in filesToInsert)
            {
                CompanyXml company = DeserializeFile(file);
                companies.Add(company);
            }

            return companies;
        }
    }
}
