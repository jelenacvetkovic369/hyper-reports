﻿using HyperReports.DAL.Model;
using HyperReports.DAL.Repositories;
using HyperReports.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.BLL.Services
{
    public class CompanyService : BaseService<Company, CompanyXml, CompanyRepository>
    {
        private readonly StoreService _storeService;

        public CompanyService(CompanyRepository repository,
            StoreService storeService)
            : base(repository)
        {
            _storeService = storeService;
        }

        public override int Insert(CompanyXml company,string comanyIdentifierName)
        {
            int companyId = base.Insert(company, comanyIdentifierName);
            foreach (StoreXml store in company.Stores)
            {
                _storeService.Insert(store, companyId);
            }
            return companyId;
        }
        
    }

}
