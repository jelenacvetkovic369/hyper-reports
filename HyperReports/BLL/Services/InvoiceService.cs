﻿using HyperReports.DAL.Model;
using HyperReports.DAL.Repositories;
using HyperReports.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.BLL.Services
{
    public class InvoiceService : BaseService<Invoice, InvoiceXml, InvoiceRepository>
    {
        private readonly CardService _cardService;
        private readonly CustomerService _customerService;

        public InvoiceService(InvoiceRepository repository,
            CardService cardService, CustomerService customerService)
            : base(repository)
        {
            _cardService = cardService;
            _customerService = customerService;
        }

        public int Insert(InvoiceXml entity, int storeId) 
        {
            Invoice invoice = _repository.Convert(entity);
            invoice.StoreUuid = storeId;
            invoice.CardUuid = null;
            if (entity.CardDetails != null)
            {
                invoice.CardUuid = _cardService.Insert(entity.CardDetails, "Number");
            }
            _customerService.Insert(entity.CustomerDetails,"Uuid");
            return _repository.Insert(invoice);
        }
    }
}
