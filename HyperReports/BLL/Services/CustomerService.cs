﻿using HyperReports.DAL.Model;
using HyperReports.DAL.Repositories;
using HyperReports.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.BLL.Services
{
    public class CustomerService : BaseService<Customer, CustomerXml, CustomerRepository>
    {

        public CustomerService(CustomerRepository repository)
            : base(repository)
        { }

    }
}
