﻿using HyperReports.DAL.Model;
using HyperReports.DAL.Repositories;
using HyperReports.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.BLL.Services
{
    public class CardService : BaseService<Card, CardXml, CardRepository>
    {

        public CardService(CardRepository repository)
            : base(repository)
        { }

    }
}
