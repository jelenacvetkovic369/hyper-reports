﻿using HyperReports.DAL.Entities;
using HyperReports.DAL.RepositoryInterfaces;
using System;

namespace HyperReports.BLL.Services
{
    public abstract class BaseService<TEntity, TEntityXml, TRepository> where TEntity : BaseEntity
        where TRepository : IBaseRepository<TEntity, TEntityXml>
    {
        protected readonly TRepository _repository;

        protected BaseService(TRepository repository)
        {
            _repository = repository;
        }

        public virtual int Insert(TEntityXml entity, string identifier)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("The Entity must not be null.");
            }
            int id = GetIdIfExists(entity, identifier);
            if (id == -1)
                id= _repository.Insert(_repository.Convert(entity));
            return id;
        }

        public int GetIdIfExists(TEntityXml entity, string identifier)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("The Entity must not be null.");
            }
            if (string.IsNullOrEmpty(identifier))
            {
                throw new ArgumentNullException("Identifier for entity must be provided.");
            }

            return _repository.GetIdIfExists(entity, identifier);

        }


    }

}
