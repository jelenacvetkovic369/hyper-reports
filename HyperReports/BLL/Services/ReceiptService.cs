﻿using HyperReports.DAL.Model;
using HyperReports.DAL.Repositories;
using HyperReports.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.BLL.Services
{
    public class ReceiptService : BaseService<Receipt, ReceiptXml, ReceiptRepository>
    {
        private readonly CardService _cardService;

        public ReceiptService(ReceiptRepository repository,
            CardService cardService)
            : base(repository)
        {
            _cardService = cardService;
        }

        public int Insert(ReceiptXml entity, int storeId)
        {
            Receipt receipt = _repository.Convert(entity);
            receipt.StoreUuid = storeId;
            receipt.CardUuid = null;
            if (entity.CardDetails != null)
            {
                receipt.CardUuid = _cardService.Insert(entity.CardDetails, "Number");
            }

            return _repository.Insert(receipt);
        }
    }
}
