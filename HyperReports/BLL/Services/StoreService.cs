﻿using HyperReports.DAL.Model;
using HyperReports.DAL.Repositories;
using HyperReports.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.BLL.Services
{
    public class StoreService : BaseService<Store, StoreXml, StoreRepository>
    {
        private readonly ReceiptService _receiptService;
        private readonly InvoiceService _invoiceService;
        public StoreService(StoreRepository repository,
            ReceiptService receiptService, InvoiceService invoiceService)
            : base(repository)
        {
            _receiptService = receiptService;
            _invoiceService = invoiceService;
        }

        public int Insert(StoreXml entity, int companyId)
        {
            int storeId = base.GetIdIfExists(entity, "Name");
            if (storeId == -1)
            {
                Store store = _repository.Convert(entity);
                store.companyUuid = companyId; 
                storeId = _repository.Insert(store);
            }

            //inserting receipts 
            foreach (ReceiptXml receipt in entity.Receipts)
            {
                _receiptService.Insert(receipt,storeId);
            }

            //inserting invoices
            foreach (InvoiceXml invoice in entity.Invoices)
            {
                _invoiceService.Insert(invoice,storeId);
            }
            return storeId;
        }

    }

}
