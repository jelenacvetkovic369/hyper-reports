﻿using ClosedXML.Excel;
using Commander.NET.Attributes;
using Commander.NET.Interfaces;
using HyperReports.DAL.Repositories;
using System;
using System.Data;
using System.IO;

namespace HyperReports.BLL.CommandLineInterface
{
    public class Report : ICommand
    {
        CompanyRepository companyRepository;

        [Parameter("--company")]
        public string Company { get; set; }

        [Parameter("--month", Required=Required.No)]
        public int Month { get; set; }

        [Parameter("--quarter", Regex = "q1|q2|q3|q4", Required = Required.No)]
        public string Quarter { get; set; } = "";

        [Parameter("--year")]
        public int Year { get; set; }

        [Parameter("--aggregation", Regex = "store|payment")]
        public string Aggregation { get; set; }

        [Parameter("--top", Required = Required.No)]
        public int N { get; set; }

        [Parameter("--order", Regex = "asc|desc", Required = Required.No)]
        public string Order { get; set; } = "";

        
        public void ExecuteReport()
        {
            if (string.IsNullOrEmpty(Settings.ExportDirectory))
                throw new ArgumentNullException("", "Error: ExportDirectory must be set. \nPlease set export directory with: hyper-reports config --export-dir <path-to-export-dir>");
            
            companyRepository = new CompanyRepository(Settings.DefaultConnection);

            int[] months = GetMonths();
            string queryString = companyRepository.CreateQueryString(N, months, Aggregation, Company, Year, Order);

            DataTable queryResult = companyRepository.GetTurnover(queryString);
            Export(queryResult);
        }
        public void Export(DataTable dt)
        {
            string period = (Month == 0) ? Quarter : Month.ToString();
            string fileName = "report-" + period + "-" + Company + "-" + Aggregation + ".xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "Company");
                string exportPath = Path.Combine(Settings.ExportDirectory, fileName);
                wb.SaveAs(exportPath, false);
            }
            Console.WriteLine("File " + fileName + " created in " + Settings.ExportDirectory);
        }
        public int[] GetMonths()
        {
            switch (Quarter)
            {
                case "q1":
                    return new int[] { 1,2,3};
                    break;
                case "q2":
                    return new int[] { 4, 5, 6 };
                    break;
                case "q3":
                    return new int[] { 7, 8, 9};
                    break;
                case "q4":
                    return new int[] { 10,11,12};
                    break;
            }
            if (Month != 0)
                return new int[] { Month };
            else
                return new int[0];
                
        }
        void ICommand.Execute(object parent)
        {
            // The "report" command was issued
        }
    }
}
