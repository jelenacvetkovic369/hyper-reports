﻿using Commander.NET.Interfaces;
using HyperReports.BLL.Services;
using HyperReports.DAL.Model;
using HyperReports.DAL.Repositories;
using System;
using System.Collections.Generic;

namespace HyperReports.BLL.CommandLineInterface
{
    public class Process :ICommand
    {
        public void InsertToDb()
        {
            Console.WriteLine("Please wait..");
            XmlDeserialization xmlFiles = new XmlDeserialization();
                List<CompanyXml> companies = xmlFiles.GetDataForDb();

                if (companies.Count > 0)
                {
                    CompanyRepository companyRepository = new CompanyRepository(Settings.DefaultConnection);
                    StoreRepository storeRepository = new StoreRepository(Settings.DefaultConnection);
                    ReceiptRepository receiptRepository = new ReceiptRepository(Settings.DefaultConnection);
                    InvoiceRepository invoiceRepository = new InvoiceRepository(Settings.DefaultConnection);
                    CardRepository cardRepository = new CardRepository(Settings.DefaultConnection);
                    CustomerRepository customerRepository = new CustomerRepository(Settings.DefaultConnection);

                    CustomerService customerService = new CustomerService(customerRepository);
                    CardService cardService = new CardService(cardRepository);
                    ReceiptService receiptService = new ReceiptService(receiptRepository, cardService);
                    InvoiceService invoiceService = new InvoiceService(invoiceRepository, cardService, customerService);
                    StoreService storeService = new StoreService(storeRepository, receiptService, invoiceService);
                    CompanyService companyService = new CompanyService(companyRepository, storeService);


                    foreach (CompanyXml company in companies)
                    {
                        companyService.Insert(company, "Uuid");
                    }
                    Console.WriteLine("Insertion complete!");
                }
                else
                    Console.WriteLine("No new files.");
            
        }
        void ICommand.Execute(object parent)
        {
            // The "process" command was issued
        }
    }
}
