﻿using Commander.NET.Attributes;
using Commander.NET.Interfaces;
using System;
using System.IO;

namespace HyperReports.BLL.CommandLineInterface
{
    public class Config : ICommand
    {
        [Parameter("--data-dir", Required = Required.No, Description = "Directory where the data files are stored locally.")]  // Required.Default
        public string DataDirectory;

        [Parameter("--export-dir", Required = Required.No, Description = "Directory in which the exported xlsx files will be stored.")]  // Required.Default
        public string ExportDirectory;

        void ICommand.Execute(object parent)
        {
            // The "config" command was issued
        }

        public void SetPath()
        {
            if (DataDirectory != null)
            {      
                Settings.LocalDirectory = DataDirectory;
                Console.WriteLine("Local directory path set to" + Settings.LocalDirectory);
            }
            else if (ExportDirectory != null)
            {
                Settings.ExportDirectory = ExportDirectory;
                if (!Directory.Exists(Settings.ExportDirectory))
                    Directory.CreateDirectory(Settings.ExportDirectory);

                Console.WriteLine("Export directory path set to " + Settings.ExportDirectory);
            }
            else
                Console.WriteLine("Directory configuration was unsuccessfull.\n");
        }
    }
}
