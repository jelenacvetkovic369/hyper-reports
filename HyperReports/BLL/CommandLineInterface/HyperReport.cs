﻿using Commander.NET.Attributes;

namespace HyperReports.BLL.CommandLineInterface
{
    public class HyperReport
    {
        [Command("config")]
        public Config Config;

        [Command("process")]
        public Process Process;

        [Command("report")]
        public Report Report;

        [CommandHandler]
        public void Command(Config config)
        {
            config.SetPath();
        }

        [CommandHandler]
        public void Command(Process process)
        {
            process.InsertToDb();
        }

        [CommandHandler]
        public void Command(Report report)
        {
            report.ExecuteReport();
        }
    }
}
